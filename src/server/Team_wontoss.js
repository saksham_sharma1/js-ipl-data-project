const fileConvert=require("../server/convert")


function Teamwon(marr) {
    const ans = {};

    for (let i = 0; i < marr.length; i++) {
        const data = marr[i];
        const tosswinner = data.toss_winner;
        const winner = data.winner;

        if (tosswinner === winner) {
            if (!ans[winner]) {
                ans[winner] = 0;
            }

            ans[winner] += 1;
        }
    }

    console.log(ans);
    fileConvert.write("Team_wontoss",ans)

}
fileConvert.getSingleData("matches",Teamwon)
