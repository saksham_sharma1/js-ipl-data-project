const fileConvert=require("../server/convert")

const solve = (darr) => {
  const issuperover = [];

  for (let i = 0; i < darr.length; i++) {
    const match = darr[i];
    if (match.is_super_over === '1') {
      issuperover.push(match);
    }
  }

  const runsdata = {};

  for (let i = 0; i < issuperover.length; i++) {
    const data = issuperover[i];
    const totalruns = parseInt(data.total_runs);
    const bowler = data.bowler;

    // These extras need to be included in the runs but excluded in the balls
    const Extras =
      data.wide_runs === '1' ||
      data.noball_runs === '1' ||
      data.bye_runs > 0 ||
      data.legbye_runs > 0;

    if (!Extras) {
      if (!runsdata[bowler]) {
        runsdata[bowler] = { runs: 0, balls: 0 };
      }
      runsdata[bowler].runs += totalruns;
      runsdata[bowler].balls += 1;
    }
  }

  const ecorate = [];

  for (let bowler in runsdata) {
    const { runs, balls } = runsdata[bowler];
    const economyrate = (runs / balls) * 6;
    ecorate.push({ bowler, economyrate });
  }

  // Sorting top 10
  ecorate.sort((a, b) => a.economyrate - b.economyrate);

  const top10 = ecorate[0];

  console.log(top10)
  fileConvert.write("best_Economy_insuperover",top10)

};


fileConvert.getSingleData("deliveries",solve)