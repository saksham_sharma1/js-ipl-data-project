const fileConvert=require("../server/convert")

function solve(darr, marr) {
    const runsdata = {};
  
    for (let i = 0; i < darr.length; i++) {
        const data = darr[i];
        const bowlingteam = data.bowling_team;
        const matchid = data.match_id;
        const extraruns = parseInt(data.extra_runs);

        let matched = null;
        for (let j = 0; j < marr.length; j++) {
            const match = marr[j];
            if (match.id === matchid && match.season === "2016") {
                matched = match;
                break;
            }
        }

        if (matched) {
            if (!runsdata[bowlingteam]) {
                runsdata[bowlingteam] = 0;
            }
            runsdata[bowlingteam] += extraruns;
        }
    }
  
    console.log(runsdata)
  fileConvert.write("extraruns_peryear",runsdata)
}



fileConvert.getBothData("deliveries","matches",solve)

