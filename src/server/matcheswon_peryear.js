const fileConvert=require("../server/convert")

function solve(narr) {
    const sol = {};
    for (let i = 0; i < narr.length; i++) {
        const data = narr[i];
        const season = data.season;
        const winner = data.winner;
        if (!sol[season]) {
            sol[season] = {};
        }
        if (winner !== "") {
            if (!sol[season][winner]) {
                sol[season][winner] = 1;
            } else {
                sol[season][winner]++;
            }
        }
    }
 
    console.log(sol)
    fileConvert.write("matches_wonperyear",sol)

}


fileConvert.getSingleData("matches",solve)