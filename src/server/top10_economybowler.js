const fileConvert=require("../server/convert")

// acc=delivery
// index =data

const solve = (marr, darr) => {
  const runsdata = {};

  for (let i = 0; i < darr.length; i++) {
    const data = darr[i];
    const matchid = data.match_id;

    let ifmatched = null;
    for (let j = 0; j < marr.length; j++) {
      const match = marr[j];
      if (match.id === matchid && match.season === "2015") {
        ifmatched = match;
        break;
      }
    }

    if (ifmatched) {
      const totalruns = parseInt(data.total_runs);
      const bowler = data.bowler;

      // These extras need to be included in the runs but excluded in the balls
      const Extras =
        data.wide_runs === "1" ||
        data.noball_runs === "1" ||
        data.bye_runs > 0 ||
        data.legbye_runs > 0;

      if (!Extras) {
        if (!runsdata[bowler]) {
          runsdata[bowler] = { runs: 0, balls: 0 };
        }
        runsdata[bowler].runs += totalruns;
        runsdata[bowler].balls += 1;
      }
    }
  }

  const ecorate = [];

  for (let bowler in runsdata) {
    const { runs, balls } = runsdata[bowler];
    const economyrate = (runs / balls) * 6;
    ecorate.push({ bowler, economyrate });
  }

  // Sorting top 10
  ecorate.sort((a, b) => a.economyrate - b.economyrate);

  const top10 = ecorate.slice(0, 10);

  fileConvert.write("Top10_economy",top10)
};

fileConvert.getBothData("matches","deliveries",solve)